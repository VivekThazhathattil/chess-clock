import 'package:chess_clock/pages/homepage.dart';
import 'package:flutter/material.dart';

import 'components/app_colors.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Vivek\'s Chess Clock',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: AppColors.primaryWhite,
      ),
      home: HomePage(),
    );
  }
}
