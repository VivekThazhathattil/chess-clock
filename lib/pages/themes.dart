import 'package:chess_clock/components/constants.dart';
import 'package:chess_clock/components/theme_tile.dart';
import 'package:flutter/material.dart';

class ThemesMenu extends StatefulWidget {
  final Function updateThemePref;
  int colorIdx;
  ThemesMenu(
      {super.key, required this.updateThemePref, required this.colorIdx});

  @override
  State<ThemesMenu> createState() => _ThemesMenuState();
}

class _ThemesMenuState extends State<ThemesMenu> {
  List<ThemeTile> _themeTiles = [];
  List<bool> checkVisibility = [];

  void _setClickedColor(int itemNumber) {
    setState(() {
      widget.colorIdx = itemNumber;
      _resetCheckVisibility();
      checkVisibility[itemNumber] = true;
      _createThemeTilesList();
    });
  }

  void _resetCheckVisibility() {
    List<bool> templist = [];
    for (int i = 0; i < kTopColors.length; ++i) {
      if (widget.colorIdx == i) {
        templist.add(true);
      } else {
        templist.add(false);
      }
    }
    setState(() {
      checkVisibility = templist;
    });
  }

  void _createThemeTilesList() {
    _themeTiles = [];
    for (int i = 0; i < kTopColors.length; ++i) {
      ThemeTile themeTile = ThemeTile(
        topColor: kTopColors[i],
        bottomColor: kBotColors[i],
        heroTag: 'ttht$i',
        setPreviewColor: _setClickedColor,
        isVisible: checkVisibility[i],
      );
      _themeTiles.add(themeTile);
    }
  }

  @override
  void initState() {
    _resetCheckVisibility();
    _createThemeTilesList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Themes'),
      ),
      body: Card(
        elevation: 2.0,
        child: Container(
            padding: const EdgeInsets.only(top: 12.0),
            child: Center(
              child: Column(
                children: [
                  Stack(
                    alignment: Alignment.center,
                    children: [
                      // preview color containers
                      ClipRRect(
                        borderRadius: BorderRadius.circular(24),
                        child: Card(
                          elevation: 2.0,
                          margin: EdgeInsets.zero,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              AnimatedContainer(
                                  duration: const Duration(milliseconds: 500),
                                  width: size.width / 2,
                                  height: size.height / 6,
                                  color: kTopColors[widget.colorIdx]),
                              AnimatedContainer(
                                  duration: const Duration(milliseconds: 500),
                                  width: size.width / 2,
                                  height: size.height / 6,
                                  color: kBotColors[widget.colorIdx]),
                            ],
                          ),
                        ),
                      ),
                      Align(
                        alignment: Alignment.center,
                        child: SizedBox(
                          width: 30,
                          child: FloatingActionButton(
                              onPressed: () {},
                              backgroundColor: Colors.white,
                              heroTag: 'fabhtrandom',
                              child: const Icon(
                                Icons.pause,
                                size: 15,
                              )),
                        ),
                      )
                    ],
                  ),
                  const Padding(
                    padding: EdgeInsets.symmetric(vertical: 24),
                    child: Divider(
                      indent: 15.0,
                      endIndent: 15.0,
                    ),
                  ),
                  Expanded(
                    flex: 5,
                    child: GridView.count(
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        crossAxisCount: 4,
                        children: [..._themeTiles]),
                  ),
                  SizedBox(
                    width: size.width * 0.8,
                    child: TextButton(
                      onPressed: () {
                        widget.updateThemePref(widget.colorIdx);
                        Navigator.of(context).pop();
                      },
                      child: AnimatedContainer(
                        width: MediaQuery.of(context).size.width * 0.8,
                        padding: const EdgeInsets.all(18),
                        margin: const EdgeInsets.all(0),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(40),
                          color: kTopColors[widget.colorIdx],
                        ),
                        duration: const Duration(milliseconds: 500),
                        child: const Text(
                          'Apply',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ),
                  const Spacer(),
                ],
              ),
            )),
      ),
    );
  }
}
