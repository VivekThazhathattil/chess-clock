import 'package:flutter/material.dart';

class SettingsMenuInfo extends StatelessWidget {
  final Color color;
  const SettingsMenuInfo({super.key, required this.color});

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Image.asset('assets/images/info_main.png'),
      Container(
        padding: const EdgeInsets.all(24),
        child: Text(
          'Thank you for choosing our product. Our team is always happy to help you!',
          style: Theme.of(context).textTheme.headline5,
        ),
      ),
      const Spacer(),
      OutlinedButton(
          onPressed: () {},
          child: Text(
            'SEND FEEDBACK',
            style: TextStyle(
              color: color,
              fontSize: 20,
            ),
          )),
      const Spacer(),
    ]);
  }
}
