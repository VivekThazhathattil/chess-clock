import 'package:flutter/material.dart';

class SettingsMenuAudio extends StatelessWidget {
  final Color color;
  const SettingsMenuAudio({super.key, required this.color});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView(
        children: [
          ListTile(
            title: const Text('Sound'),
            subtitle: const Text('Beep after each move'),
            trailing: Switch(
              activeColor: color,
              onChanged: (_) {},
              value: true,
            ),
          ),
          const Divider(),
          ListTile(
            title: const Text('Low time warning'),
            trailing: Switch(
              activeColor: color,
              onChanged: (_) {},
              value: true,
            ),
          ),
          ListTile(
            title: const Text('Vibrate'),
            trailing: Switch(
              activeColor: color,
              onChanged: (_) {},
              value: true,
            ),
          ),
          ListTile(
            title: const Text('Alert Time'),
            subtitle: const Text('17:00:00'),
          ),
        ],
      ),
    );
  }
}
