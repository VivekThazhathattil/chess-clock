import 'package:chess_clock/components/game_mode_tile.dart';
import 'package:chess_clock/pages/settings_menu_audio.dart';
import 'package:chess_clock/pages/settings_menu_info.dart';
import 'package:circular_bottom_navigation/circular_bottom_navigation.dart';
import 'package:circular_bottom_navigation/tab_item.dart';
import 'settings_menu_gamepad.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class SettingsMenu extends StatefulWidget {
  var colTop, colBot;
  SettingsMenu({
    super.key,
    required this.colTop,
    required this.colBot,
  });

  @override
  State<SettingsMenu> createState() => _SettingsMenuState();
}

class _SettingsMenuState extends State<SettingsMenu> {
  int? selectedPos;
  CircularBottomNavigationController? circularBottomNavigationController;
  Widget _currDisplayWidget = Container();

  @override
  void initState() {
    selectedPos = 1;
    circularBottomNavigationController =
        CircularBottomNavigationController(selectedPos);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    List<TabItem> tabItems = <TabItem>[
      TabItem(
        Icons.notifications,
        'AUDIO',
        widget.colTop,
      ),
      TabItem(
        Icons.gamepad,
        'GAME MODE',
        widget.colTop,
      ),
      TabItem(
        Icons.info,
        'INFO',
        widget.colTop,
      ),
    ];

    //set desired tab based on selectedPos
    switch (selectedPos) {
      case 0:
        setState(() {
          _currDisplayWidget = SettingsMenuAudio(
            color: widget.colTop,
          );
        });
        break;
      case 1:
        setState(() {
          _currDisplayWidget = SettingsMenuGamePad(
            colTop: widget.colTop,
            colBot: widget.colBot,
          );
        });
        break;
      case 2:
        setState(() {
          _currDisplayWidget = SettingsMenuInfo(
            color: widget.colTop,
          );
        });
        break;
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text('Settings'),
      ),
      body: Stack(
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            child: _currDisplayWidget,
          ),
          Column(
            children: [
              Expanded(flex: 1, child: Container()),
              CircularBottomNavigation(
                tabItems,
                selectedPos: selectedPos!,
                controller: circularBottomNavigationController,
                selectedCallback: (int? sp) {
                  setState(() {
                    this.selectedPos = sp ?? 0;
                  });
                },
              ),
            ],
          )
        ],
      ),
    );
  }
}
