import 'package:chess_clock/components/game_mode_tile.dart';
import 'package:circular_bottom_navigation/circular_bottom_navigation.dart';
import 'package:circular_bottom_navigation/tab_item.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class SettingsMenuGamePad extends StatefulWidget {
  var colTop, colBot;
  SettingsMenuGamePad({
    super.key,
    required this.colTop,
    required this.colBot,
  });

  @override
  State<SettingsMenuGamePad> createState() => _SettingsMenuGamePadState();
}

class _SettingsMenuGamePadState extends State<SettingsMenuGamePad> {
  List<GameModeTile> gameModeTiles = [];

  List<Map> gameModes = [
    {
      'name': 'Active',
      'mins': 30,
    },
    {
      'name': 'Classical',
      'mins': 15,
    },
    {
      'name': 'Rapid',
      'mins': 10,
    },
    {
      'name': 'Blitz',
      'mins': 5,
    },
    {
      'name': 'Bullet I',
      'mins': 3,
    },
    {
      'name': 'Bullet II',
      'mins': 1,
    },
    {
      'name': 'Custom I',
      'mins': 0,
    },
    {
      'name': 'Custom II',
      'mins': 0,
    },
  ];

  void _buildGameModeTilesList() {
    List<GameModeTile> _gameModeTiles = [];
    for (int i = 0; i < gameModes.length; ++i) {
      GameModeTile gameModeTile = GameModeTile(
        modeName: gameModes[i]['name'],
        timeMins: gameModes[i]['mins'],
        accentColor: Colors.grey[200],
        textColor: Colors.black87,
        gameModeId: i,
        activateMyColors: _activateColors,
      );
      _gameModeTiles.add(gameModeTile);
    }
    setState(() {
      gameModeTiles = _gameModeTiles;
    });
  }

  void _activateColors(int i) {
    _buildGameModeTilesList();
    GameModeTile gameModeTile = GameModeTile(
      modeName: gameModes[i]['name'],
      timeMins: gameModes[i]['mins'],
      accentColor: widget.colTop,
      textColor: Colors.grey[200],
      gameModeId: i,
      activateMyColors: _activateColors,
    );
    setState(() {
      gameModeTiles[i] = gameModeTile;
    });
  }

  int selectedPos = 1;
  CircularBottomNavigationController? circularBottomNavigationController;

  @override
  void initState() {
    super.initState();
    _buildGameModeTilesList();
    circularBottomNavigationController =
        CircularBottomNavigationController(selectedPos);
  }

  @override
  Widget build(BuildContext context) {
    return GridView.count(
      childAspectRatio: 0.9,
      crossAxisCount: 2,
      padding: const EdgeInsets.symmetric(horizontal: 8),
      crossAxisSpacing: 4.0,
      mainAxisSpacing: 2.0,
      children: gameModeTiles,
    );
  }
}
