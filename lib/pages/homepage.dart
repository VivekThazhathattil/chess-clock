import 'package:chess_clock/components/player_clock.dart';
import 'package:chess_clock/pages/settings_menu.dart';
import 'package:chess_clock/pages/themes.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

import '../components/constants.dart';
import '../components/time_text.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int fracTop = 1, fracBottom = 1, colorIdx = 0;
  String whiteTimeString = '30:00', blackTimeString = '30:00';
  PlayerClock? pclock;

  @override
  void initState() {
    colorIdx = 0;
    pclock = PlayerClock(
      updateWhiteString: _updateWhiteClock,
      updateBlackString: _updateBlackClock,
    );
    super.initState();
  }

  void _setThemePreference(int colIdx) {
    setState(() {
      colorIdx = colIdx;
    });
  }

  void _updateWhiteClock(String whiteClockTimeString) {
    setState(() => whiteTimeString = whiteClockTimeString);
  }

  void _updateBlackClock(String blackClockTimeString) {
    setState(() => blackTimeString = blackClockTimeString);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          child: Stack(
        children: [
          Column(
            children: [
              GestureDetector(
                onTap: () {
                  setState(() {
                    if (fracTop == 1 && fracBottom == 1) {
                      pclock!.startWhitesClock();
                      fracTop = 3;
                      fracBottom = 1;
                    } else if (fracTop == 3) {
                      fracTop = 1;
                      fracBottom = 3;
                      pclock!.startBlacksClock();
                    }
                  });
                },
                child: AnimatedContainer(
                  height: MediaQuery.of(context).size.height *
                      fracTop /
                      (fracTop + fracBottom),
                  width: MediaQuery.of(context).size.width,
                  color: kTopColors[colorIdx],
                  duration: const Duration(milliseconds: 300),
                  curve: Curves.easeOut,
                  child: RotatedBox(
                    quarterTurns: 2,
                    child: TimeText(
                      clockTimeText: blackTimeString,
                      instructText: pclock == null || !pclock!.isPlaying
                          ? 'Tap to start'
                          : '',
                    ),
                  ),
                ),
              ),
              const Spacer(),
              GestureDetector(
                onTap: () {
                  setState(() {
                    if (fracTop == 1 && fracBottom == 1) {
                      fracTop = 3;
                      fracBottom = 1;
                    } else if (fracBottom == 3) {
                      fracBottom = 1;
                      fracTop = 3;
                    }
                    pclock!.startWhitesClock();
                  });
                },
                child: AnimatedContainer(
                  height: MediaQuery.of(context).size.height *
                      fracBottom /
                      (fracTop + fracBottom),
                  width: MediaQuery.of(context).size.width,
                  color: kBotColors[colorIdx],
                  duration: const Duration(milliseconds: 300),
                  curve: Curves.easeOut,
                  child: TimeText(
                    clockTimeText: whiteTimeString,
                    instructText: pclock == null || !pclock!.isPlaying
                        ? 'Tap to start'
                        : '',
                  ),
                ),
              ),
            ],
          ),
          AnimatedPositioned(
            duration: const Duration(milliseconds: 300),
            curve: Curves.easeOut,
            top: fracTop /
                    (fracTop + fracBottom) *
                    MediaQuery.of(context).size.height -
                25,
            child: SizedBox(
              width: MediaQuery.of(context).size.width,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  FloatingActionButton(
                    heroTag: 'settingsHeroTag',
                    backgroundColor: Colors.white,
                    onPressed: () {
                      Navigator.push(
                          context,
                          PageTransition(
                            type: PageTransitionType.leftToRight,
                            child: SettingsMenu(
                              colTop: kTopColors[colorIdx],
                              colBot: kBotColors[colorIdx],
                            ),
                          ));
                    },
                    child: const Icon(
                      Icons.settings,
                      color: Colors.black,
                      size: 30,
                    ),
                  ),
                  Visibility(
                    visible: pclock!.playing,
                    child: FloatingActionButton(
                      heroTag: 'pauseHeroTag',
                      backgroundColor: Colors.white,
                      onPressed: () {
                        pclock!.pauseClocks();
                      },
                      child: const Icon(
                        Icons.pause,
                        color: Colors.black,
                        size: 30,
                      ),
                    ),
                  ),
                  FloatingActionButton(
                    heroTag: 'paletteHeroTag',
                    backgroundColor: Colors.white,
                    onPressed: () {
                      Navigator.push(
                          context,
                          PageTransition(
                              type: PageTransitionType.rightToLeft,
                              child: ThemesMenu(
                                updateThemePref: _setThemePreference,
                                colorIdx: colorIdx,
                              )));
                    },
                    child: const Icon(
                      Icons.palette,
                      color: Colors.black,
                      size: 30,
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      )),
    );
  }
}
