import 'package:flutter/material.dart';

class ControlButtons extends StatelessWidget {
  final int flexTop, flexBottom;
  const ControlButtons(
      {super.key, required this.flexTop, required this.flexBottom});

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        alignment: Alignment(0, 0.33 + 25 / MediaQuery.of(context).size.height),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: const [
            Icon(
              Icons.settings,
              color: Colors.white,
              size: 50,
            ),
            Icon(
              Icons.restart_alt,
              color: Colors.white,
              size: 50,
            ),
            Icon(
              Icons.palette,
              color: Colors.white,
              size: 50,
            ),
          ],
        ),
      ),
    );
  }
}
