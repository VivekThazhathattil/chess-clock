import 'package:flip_card/flip_card.dart';
import 'package:flip_card/flip_card_controller.dart';
import 'package:flutter/material.dart';

class GameModeTile extends StatefulWidget {
  final modeName;
  final int timeMins;
  final accentColor, textColor;
  final int gameModeId;
  final Function activateMyColors;
  const GameModeTile({
    super.key,
    required this.modeName,
    required this.timeMins,
    required this.accentColor,
    required this.textColor,
    required this.gameModeId,
    required this.activateMyColors,
  });

  @override
  State<GameModeTile> createState() => _GameModeTileState();
}

class _GameModeTileState extends State<GameModeTile> {
  FlipCardController? _controller;
  @override
  void initState() {
    _controller = FlipCardController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () {
        widget.activateMyColors(widget.gameModeId);
      },
      child: Padding(
          padding: const EdgeInsets.only(top: 8.0),
          child: FlipCard(
            controller: _controller,
            flipOnTouch: false,
            front: GameModeFrontCard(
              accentColor: widget.accentColor,
              textColor: widget.textColor,
              modeName: widget.modeName,
              size: size,
              timeMins: widget.timeMins,
              controller: _controller!,
            ),
            back: GameModeBackCard(
                accentColor: widget.accentColor,
                textColor: widget.textColor,
                controller: _controller,
                timeMins: widget.timeMins),
          )

          ////
          ),
    );
  }
}

class GameModeBackCard extends StatelessWidget {
  const GameModeBackCard({
    Key? key,
    required this.accentColor,
    required this.textColor,
    required this.timeMins,
    required this.controller,
  }) : super(key: key);

  final accentColor;
  final textColor;
  final int timeMins;
  final controller;

  @override
  Widget build(BuildContext context) {
    return Card(
        color: accentColor,
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  children: [
                    Expanded(flex: 1, child: Container()),
                    GestureDetector(
                      onTap: () {
                        controller.toggleCard();
                      },
                      child: Icon(
                        Icons.info,
                        color: textColor,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 10),
                Text(
                  'Total game time for each player:',
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w500,
                    color: textColor,
                  ),
                ),
                SizedBox(height: 10),
                Text(
                  '$timeMins min',
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: textColor,
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}

class GameModeFrontCard extends StatelessWidget {
  const GameModeFrontCard({
    Key? key,
    required this.accentColor,
    required this.textColor,
    required this.modeName,
    required this.size,
    required this.timeMins,
    required this.controller,
  }) : super(key: key);

  final accentColor;
  final textColor;
  final modeName;
  final Size size;
  final int timeMins;
  final FlipCardController controller;

  @override
  Widget build(BuildContext context) {
    return Card(
      color: accentColor,
      elevation: 2.0,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(children: [
            const Spacer(),
            Padding(
              padding: const EdgeInsets.only(left: 8.0, top: 8.0, right: 8.0),
              child: GestureDetector(
                onTap: () {
                  controller.toggleCard();
                },
                child: Icon(
                  Icons.info,
                  color: textColor,
                ),
              ),
            ),
          ]),
          Text(modeName,
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                color: textColor,
              )),
          Divider(
            color: textColor,
            thickness: 1,
            indent: size.width * 0.175,
            endIndent: size.width * 0.175,
          ),
          Text(
            timeMins.toString(),
            style: TextStyle(
                fontSize: 30, fontWeight: FontWeight.w900, color: textColor),
          ),
          Text(
            'min',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: textColor,
            ),
          ),
          const Spacer(),
        ],
      ),
    );
  }
}
