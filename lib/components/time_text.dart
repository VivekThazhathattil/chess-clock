import 'package:flutter/material.dart';

class TimeText extends StatelessWidget {
  final String clockTimeText;
  final String instructText;
  const TimeText({
    Key? key,
    required this.clockTimeText,
    required this.instructText,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          clockTimeText,
          style: const TextStyle(
            fontSize: 60,
            fontWeight: FontWeight.bold,
            color: Colors.white,
          ),
        ),
        Text(
          instructText,
          style: const TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
            color: Colors.white,
          ),
        )
      ],
    ));
  }
}
