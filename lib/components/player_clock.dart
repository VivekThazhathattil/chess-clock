import 'dart:async';
import 'package:flutter/material.dart';

class PlayerClock {
  Duration whiteTimeLeft;
  Duration blackTimeLeft;
  Duration whiteIncrement;
  Duration blackIncrement;
  Function? updateWhiteString, updateBlackString;

  bool playing;
  bool whitesTurn;

  int whiteNumMoves;
  int blackNumMoves;

  PlayerClock({
    this.whiteTimeLeft = const Duration(minutes: 5),
    this.blackTimeLeft = const Duration(minutes: 5),
    this.whiteIncrement = const Duration(seconds: 0),
    this.blackIncrement = const Duration(seconds: 0),
    this.updateWhiteString,
    this.updateBlackString,
    this.playing = false,
    this.whitesTurn = true,
    this.whiteNumMoves = 0,
    this.blackNumMoves = 0,
  }) {
    defaultWhiteTime = whiteTimeLeft;
    defaultBlackTime = blackTimeLeft;
  }

  Timer? whiteTimer, blackTimer;
  Duration? defaultWhiteTime, defaultBlackTime;

  String getTimestring(Duration duration) {
    String timeString = duration.toString();
    if (duration < const Duration(hours: 1)) {
      timeString = timeString.substring(2);
    }
    return timeString.split('.')[0];
  }

  void startWhitesClock() {
    if (blackTimer != null && blackTimer!.isActive) blackTimer!.cancel();
    if (whiteTimer != null && whiteTimer!.isActive) return;
    if (!playing) playing = true;
    whiteTimer = Timer.periodic(const Duration(milliseconds: 100), (timer) {
      if (whiteTimeLeft <= const Duration(seconds: 0)) {
        timer.cancel();
      } else {
        whiteTimeLeft -= const Duration(milliseconds: 100);
        updateWhiteString!(getTimestring(whiteTimeLeft));
      }
    });
  }

  get isPlaying => playing;

  void startBlacksClock() {
    if (blackTimer != null && blackTimer!.isActive) return;
    if (whiteTimer != null && whiteTimer!.isActive) whiteTimer!.cancel();
    if (!playing) playing = true;
    blackTimer = Timer.periodic(const Duration(milliseconds: 100), (timer) {
      if (blackTimeLeft <= const Duration(seconds: 0)) {
        timer.cancel();
      } else {
        blackTimeLeft -= const Duration(milliseconds: 100);
        updateBlackString!(getTimestring(blackTimeLeft));
      }
    });
  }

  // TODO: action for clock timeout
  // TODO: White text on white background idiocy
  // TODO: Pause button

  void pauseClock(timer) {
    if (timer != null && timer!.isActive) {
      timer!.cancel();
    }
  }

  void pauseClocks() {
    //if (whiteTimer != null && whiteTimer!.isActive) {
    //  whiteTimer!.cancel();
    //}
    //if (blackTimer != null && blackTimer!.isActive) {
    //  blackTimer!.cancel();
    //}
    pauseClock(whiteTimer);
    pauseClock(blackTimer);
  }

  void resetClocks() {
    whiteTimeLeft = defaultWhiteTime!;
    blackTimeLeft = defaultBlackTime!;
    if (whiteTimer != null && whiteTimer!.isActive) {
      whiteTimer!.cancel();
    }
    if (blackTimer != null && blackTimer!.isActive) {
      blackTimer!.cancel();
    }
    playing = false;
    whitesTurn = true;
  }
}
