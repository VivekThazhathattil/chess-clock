import 'package:flutter/material.dart';

class TimeSlab extends StatelessWidget {
  final Duration availableTime;
  final Color slabColor;
  final int numMoves;
  final int flexNum;
  final bool isReversed;

  const TimeSlab({
    super.key,
    required this.availableTime,
    required this.slabColor,
    required this.numMoves,
    required this.flexNum,
    required this.isReversed,
  });

  String _getAvailableTimeString() {
    return availableTime.toString().substring(2, 7);
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
        flex: flexNum,
        child: Container(
          color: slabColor,
          child: RotatedBox(
            quarterTurns: isReversed ? 2 : 0,
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    child: Text(
                      _getAvailableTimeString(),
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 70,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                      alignment: Alignment(0, 0),
                      child: Text("Tap to start",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                              fontWeight: FontWeight.bold)))
                ]),
          ),
        ));
  }
}
