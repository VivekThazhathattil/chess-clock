import 'package:flutter/material.dart';

class ThemeTile extends StatelessWidget {
  final heroTag;
  final topColor, bottomColor;
  final Function setPreviewColor;
  final isVisible;
  const ThemeTile(
      {super.key,
      required this.heroTag,
      required this.topColor,
      required this.bottomColor,
      required this.setPreviewColor,
      required this.isVisible});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8),
      child: FloatingActionButton(
          heroTag: heroTag,
          clipBehavior: Clip.antiAlias,
          onPressed: () {
            setPreviewColor(int.parse(heroTag[heroTag.toString().length - 1]));
          },
          child: Padding(
            padding: const EdgeInsets.all(3),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(30),
              child: SizedBox(
                height: 100,
                width: 100,
                child: Stack(
                  children: [
                    Row(
                      children: [
                        Expanded(
                          flex: 1,
                          child: Container(
                            color: topColor,
                          ),
                        ),
                        Expanded(flex: 1, child: Container(color: bottomColor)),
                      ],
                    ),
                    Visibility(
                      visible: isVisible,
                      child: Stack(
                        children: [
                          Center(
                            child: Container(
                              color: Colors.black54,
                              height: 500,
                            ),
                          ),
                          const Center(
                            child: Icon(
                              Icons.check_circle_rounded,
                              color: Colors.white,
                              size: 50,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )),
    );
  }
}
