class ChessClock {
  int _startedAt = -1;

  ChessClock();

  void start() {
    _startedAt = DateTime.now().millisecondsSinceEpoch;
  }

  int getMillisElapsed() {
    return DateTime.now().millisecondsSinceEpoch - _startedAt;
  }

  void pause() {}

  void resume() {}

  void cancel() {}
}
