import 'package:flutter/material.dart';

const List<Color> kTopColors = [
  Color(0xFF85623E),
  Color(0xFF769656),
  Color(0xFF83A4B9),
  Color(0xFFE68773),
  Color(0xFF373737),
  Color(0xFFEF5651),
  Color(0xFFEECD1C),
  Color(0xFFBC2836),
  Color(0xFF37B68B),
];

const List<Color> kBotColors = [
  Color(0xFFC8AC7D),
  Color(0xFFEEEED2),
  Color(0xFFE7F0F9),
  Color(0xFF2B364C),
  Color(0xFFEFEEE9),
  Color(0xFF0FC3C2),
  Color(0xFF2F2F2F),
  Color(0xFF1D2225),
  Color(0xFF34323F),
];
